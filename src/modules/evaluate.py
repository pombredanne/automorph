import codecs
import datastruct.wordlist

def load_lexemes(filename):
	lexemes = {}
	with codecs.open(filename, 'r', 'utf-8') as fp:
		for line in fp:
			words = set(line.strip().split(', '))
			for word in words:
				lexemes[word] = words
	return lexemes

def run(wordlist_filename, lexemes_gs_filename):
	print 'Loading word list...'
	wordlist = datastruct.wordlist.load(wordlist_filename)
	print 'Loading lexemes...'
	lexemes = load_lexemes('lexemes.txt')
	print 'Loading golden standard lexemes...'
	lexemes_gs = load_lexemes(lexemes_gs_filename)
	print 'Evaluating...'
	tp, fp, fn = 0, 0, 0
	with codecs.open('evaluation.txt', 'w+', 'utf-8') as filep:
		for word in wordlist:
			try:
				lexeme = lexemes[word]
				lexeme_gs = lexemes_gs[word]
			except KeyError:
				continue
			l_tp = len(lexeme & lexeme_gs)
			l_fp = len(lexeme - lexeme_gs)
			l_fn = len(lexeme_gs - lexeme)
			filep.write(word + '\t' + str(l_tp) + '\t' + str(l_fp) + '\t' + str(l_fn) + '\n')
			filep.write('TP: ' + u', '.join(lexeme & lexeme_gs) + '\n')
			filep.write('FP: ' + u', '.join(lexeme - lexeme_gs) + '\n')
			filep.write('FN: ' + u', '.join(lexeme_gs - lexeme) + '\n')
			filep.write('\n')
			tp += l_tp
			fp += l_fp
			fn += l_fn
	print 'TP: ', tp
	print 'FP: ', fp
	print 'FN: ', fn
	print 'Precision: %.2f' % (100 * float(tp) / (tp + fp)), '%'
	print 'Recall: %.2f' % (100 * float(tp) / (tp + fn)), '%'
	print 'F-measure: %.2f' % (100 * float(2*tp) / (2*tp + fp + fn)), '%'
