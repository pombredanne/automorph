import datastruct.endings
import datastruct.stems
import settings
import math

# THRESHOLD VALUES:
# lat-wikt-4: 0.01
# lat-wikt-5: 0.03
# lat-wikt-6: 0.02
# lat-wikt-7: 0.015
# lat-wikt-decl: 0.05
# lat-wikt-decl-8: 0.002
# deu-wikt: 0.01
# fin-wikt-7: 0.01
# fin-wikt-6: 0.002
# pol-sjp: 0.05
# pol-sjp-2: 0.01
# pol-sjp-3: 0.002

INDEPENDANCY_THRESHOLD = 0.01

def recount_endings(endings_c, endingsets_c):
	count = 0
	for e in endings_c.n_grams.keys():
		endings_c[e].count = 0
		for es in endingsets_c.endingsets.keys():
			endings = set(es.split(','))
			if e in endings:
				endings_c[e].count += endingsets_c[es].count
		endings_c[e].frequency = float(endings_c[e].count) / endingsets_c.count
		count += 1
		if count % 100 == 0:
			print count
	return endings_c

def calculate_endings_vec(endings_c, endingsets_c):
	endings_vec = {}
	for es in endingsets_c.endingsets.values():
		endings_vec[es.key] = {}
		endings = set(es.key.split(u','))
		for e in endings_c.n_grams.keys():
			endings_vec[es.key][e] = '1' if e in endings else '0'
	return endings_vec

def calculate_ending_entropies(endings_c):
	entropies = {}
	for e in endings_c.n_grams.values():
		if e.frequency > 0:
			entropies[e.orth] = -e.frequency * math.log(e.frequency) - (1 - e.frequency) * math.log(1 - e.frequency)
	return entropies

def set_entropy(endingset, endings_list, global_subsets_c, endings_vec):
	#print 'Calculating set entropy:', ','.join(endings_list).encode('utf-8')
	local_subsets_c = {}
	local_endings_list = sorted(list(endingset))
	total_count = 0
	for gl_key, count in global_subsets_c.iteritems():
		total_count += count
		local_subset_key = ''
		for e in local_endings_list:
			local_subset_key += gl_key[endings_list.index(e)]
		try:
			local_subsets_c[local_subset_key] += count
		except KeyError:
			local_subsets_c[local_subset_key] = count
	result = 0.0
	for c in local_subsets_c.values():
		pr = float(c) / total_count
		result -= pr * math.log(pr)
	#print 'Done.'
	return result

#def set_entropy(endingset, endingsets_c, endings_vec):
#	subsets_count = {}
#	endings_list = sorted(list(endingset))
#	for es in endingsets_c.endingsets.values():
#		subset_key = ''
#		for e in endings_list:
#			subset_key += endings_vec[es.key][e]
#		try:
#			subsets_count[subset_key] += es.count
#		except KeyError:
#			subsets_count[subset_key] = es.count
#	result = 0.0
#	for c in subsets_count.values():
#		pr = float(c) / endingsets_c.count
#		result -= pr * math.log(pr)
#	return result

def key(endingset):
	return ','.join(sorted(list(endingset)))

def unkey(endings_str):
	return set(endings_str.split(','))

def split_ending_set(endings, endings_c, endingsets_c, endings_vec, entropies):
	#my_entropies = {}
	#for e in endings:
	#	my_entropies[e] = entropies[e]
	endings_list = sorted(list(endings))
	global_subsets_c= {}
	for es in endingsets_c.endingsets.values():
		global_subset_key = ''
		for e in endings_list:
			global_subset_key += endings_vec[es.key][e]
		try:
			global_subsets_c[global_subset_key] += es.count
		except KeyError:
			global_subsets_c[global_subset_key] = es.count
	sets = [set([e]) for e in endings]
	matrix = {}
	for s1 in sets:
		s1_key = key(s1)
		matrix[s1_key] = {}
		for s2 in sets:
			if s1 == s2: continue
			s12 = s1 | s2
			s2_key = key(s2)
			s12_key = key(s12)
			#my_entropies[s12_key] = set_entropy(s12, endingsets_c)
			try:
				e = entropies[s12_key]
			except KeyError:
				#entropies[s12_key] = set_entropy(s12, endingsets_c)
				entropies[s12_key] = set_entropy(s12, endings_list, global_subsets_c, endings_vec)
			#matrix[s1_key][s2_key] = my_entropies[s1_key] + my_entropies[s2_key] - my_entropies[s12_key]
			matrix[s1_key][s2_key] = entropies[s1_key] + entropies[s2_key] - entropies[s12_key]
	while True:
		max_redundancy = 0
		s_max_1, s_max_2 = None, None
		merge = {}
		for s1 in sets:
			s1_key = key(s1)
			for s2 in sets:
				if s1 == s2: continue
				s2_key = key(s2)
				try:
					if matrix[s1_key][s2_key] > merge[s1_key][1]:
						merge[s1_key] = (s2, matrix[s1_key][s2_key])
				except KeyError:
					if matrix[s1_key][s2_key] >= INDEPENDANCY_THRESHOLD:
						merge[s1_key] = (s2, matrix[s1_key][s2_key])
		merge_list = sorted([(unkey(s1_key), s2, r) for s1_key, (s2, r) in merge.iteritems()], \
			reverse = True, key = lambda x: x[2])
		# merge_list[0][2] = maximal redundancy
		if not merge_list or merge_list[0][2] < INDEPENDANCY_THRESHOLD: break
		# perform merges
		removed, added = [], []
		for s1, s2, r in merge_list:
			if not s1 in removed and not s2 in removed:
				# merge s1 and s2
				s12 = s1 | s2
				s12_key = key(s12)
				s1_key = key(s1)
				s2_key = key(s2)
				removed.append(s1)
				removed.append(s2)
				added.append(s12)
		# remove removed sets from matrix
		for s_removed in removed:
			s_removed_key = key(s_removed)
			for s in sets:
				s_key = key(s)
				try: del matrix[s_key][s_removed_key]
				except KeyError: pass
				try: del matrix[s_removed_key][s_key]
				except KeyError: pass
		# remove them from sets list
		for s_removed in removed:
			sets.remove(s_removed)
		# add added sets to sets list and to matrix
		sets.extend(added)
		for s_added in added:
			s_added_key = key(s_added)
			matrix[s_added_key] = {}
		for s_added in added:
			s_added_key = key(s_added)
			for s in sets:
				s_key = key(s)
				s_with_s_added = s_added | s
				s_with_s_added_key = key(s_with_s_added)
				try:
					e = entropies[s_with_s_added_key]
				except KeyError:
					#entropies[s_s12_key] = set_entropy(s_s12, endingsets_c)
					entropies[s_with_s_added_key] = \
						set_entropy(s_with_s_added, endings_list, global_subsets_c, endings_vec)
				entr = entropies[s_key] + entropies[s_added_key] - entropies[s_with_s_added_key]
				matrix[s_key][s_added_key] = entr
				matrix[s_added_key][s_key] = entr
	# convert string sets to Ending instance sets
	instance_sets = []
	for s in sets:
		instance_sets.append(set([endings_c[e] for e in s]))
	return instance_sets

#def split_ending_set(endings, endings_c, endingsets_c, endings_vec, entropies):
#	#my_entropies = {}
#	#for e in endings:
#	#	my_entropies[e] = entropies[e]
#	endings_list = sorted(list(endings))
#	global_subsets_c= {}
#	for es in endingsets_c.endingsets.values():
#		global_subset_key = ''
#		for e in endings_list:
#			global_subset_key += endings_vec[es.key][e]
#		try:
#			global_subsets_c[global_subset_key] += es.count
#		except KeyError:
#			global_subsets_c[global_subset_key] = es.count
#	sets = [set([e]) for e in endings]
#	matrix = {}
#	for s1 in sets:
#		s1_key = key(s1)
#		matrix[s1_key] = {}
#		for s2 in sets:
#			if s1 == s2: continue
#			s12 = s1 | s2
#			s2_key = key(s2)
#			s12_key = key(s12)
#			#my_entropies[s12_key] = set_entropy(s12, endingsets_c)
#			try:
#				e = entropies[s12_key]
#			except KeyError:
#				#entropies[s12_key] = set_entropy(s12, endingsets_c)
#				entropies[s12_key] = set_entropy(s12, endings_list, global_subsets_c, endings_vec)
#			#matrix[s1_key][s2_key] = my_entropies[s1_key] + my_entropies[s2_key] - my_entropies[s12_key]
#			matrix[s1_key][s2_key] = entropies[s1_key] + entropies[s2_key] - entropies[s12_key]
#	while True:
#		max_redundancy = 0
#		s_max_1, s_max_2 = None, None
#		for s1 in sets:
#			s1_key = key(s1)
#			for s2 in sets:
#				if s1 == s2: continue
#				s2_key = key(s2)
#				if matrix[s1_key][s2_key] > max_redundancy:
#					max_redundancy = matrix[s1_key][s2_key]
#					s_max_1, s_max_2 = s1, s2
#		# instead of max. redundancy -- take all disjoint redundancies >= IND_THR
#		if max_redundancy < INDEPENDANCY_THRESHOLD: break
#		s_max_12 = s_max_1 | s_max_2
#		s_max_12_key = key(s_max_12)
#		s_max_1_key = key(s_max_1)
#		s_max_2_key = key(s_max_2)
#		# update matrix
#		for s in sets:
#			try:
#				s_key = key(s)
#				del matrix[s_key][s_max_1_key]
#				del matrix[s_max_1_key][s_key]
#				del matrix[s_key][s_max_2_key]
#				del matrix[s_max_2_key][s_key]
#			except KeyError:
#				pass
#		sets.remove(s_max_1)
#		sets.remove(s_max_2)
#		sets.append(s_max_12)
#		matrix[s_max_12_key] = {}
#		for s in sets:
#			s_key = key(s)
#			s_s12 = s | s_max_12
#			s_s12_key = key(s_s12)
#			#my_entropies[s_s12_key] = set_entropy(s_s12, endingsets_c)
#			try:
#				e = entropies[s_s12_key]
#			except KeyError:
#				#entropies[s_s12_key] = set_entropy(s_s12, endingsets_c)
#				entropies[s_s12_key] = set_entropy(s_s12, endings_list, global_subsets_c, endings_vec)
#			#entr = my_entropies[s_key] + my_entropies[s_max_12_key] - my_entropies[s_s12_key]
#			entr = entropies[s_key] + entropies[s_max_12_key] - entropies[s_s12_key]
#			matrix[s_key][s_max_12_key] = entr
#			matrix[s_max_12_key][s_key] = entr
#	# convert string sets to Ending instance sets
#	instance_sets = []
#	for s in sets:
#		instance_sets.append(set([endings_c[e] for e in s]))
#	return instance_sets

def run():
	print 'Loading endings list...'
	endings_c = datastruct.endings.EndingsContainer.load_from_file('endings.txt', \
		settings.ENDINGS_FILTER)
	print 'Loading ending sets...'
	endingsets_c = datastruct.endingsets.EndingSetsContainer.load_from_file('endingsets.txt')
	print 'Recounting endings...'
	endings_c = recount_endings(endings_c, endingsets_c)
	print 'Calculating ending set vectors...'
	endings_vec = calculate_endings_vec(endings_c, endingsets_c)
	print 'Loading trie...'
	trie = datastruct.stems.StemTrie.load_from_file('trie.txt', endings_c)
	print 'Calculating ending entropies...'
	entropies = calculate_ending_entropies(endings_c)
	print 'Splitting ending sets...'
	count = 0
	spl_count = 0
	splittings = {}
	for stem, endingset in trie.list_stems():
		endings = set([e.orth for e in endingset])
		endings_key = key(endings)
		try:
			trie[stem].endingsets = splittings[endings_key]
		except KeyError:
			splittings[endings_key] = split_ending_set(endings, endings_c, endingsets_c, endings_vec, entropies)
			trie[stem].endingsets = splittings[endings_key]
			spl_count += 1
			if spl_count % 100 == 0:
				print '*', spl_count

		#trie[stem].endingsets = split_ending_set(endings, endings_c, endingsets_c, endings_vec, entropies)
		print len(entropies), stem.encode('utf-8'), ':::', \
			u' ::: '.join([datastruct.endingsets.EndingSet.endings_to_string(es) \
				for es in trie[stem].endingsets]).encode('utf-8')
		count += 1
		if count % 100 == 0:
			print '===', count
	print 'Saving trie...'
	trie.save_to_file('trie_stems_split.txt')
