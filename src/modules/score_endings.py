import datastruct.ngrams
import datastruct.endings
import datastruct.wordlist
import math

MAX_N_GRAM_LENGTH = 9
EMPTY_ENDING_SCORE = 0.03

def generate_n_grams(word, n):
	return [word[i:i+n] for i in range(0, len(word)-n+1)]

def run(wordlist_file):
	print 'Loading word list...'
	wordlist = datastruct.wordlist.load(wordlist_file)
	print 'Generating n-grams...'
	endings_c = datastruct.endings.EndingsContainer()
	count = 0
	for n in range(1, MAX_N_GRAM_LENGTH):
		print 'n =', n
		n_grams_c = datastruct.ngrams.NGramsContainer()
		ending_n_grams_c = datastruct.ngrams.NGramsContainer()
		for word in wordlist:
			if n <= len(word):
				ending_n_grams_c.add(word[-n:])
			for n_gr in generate_n_grams(word, n):
				n_grams_c.add(n_gr)
		n_grams_c.calculate_frequencies()
		ending_n_grams_c.calculate_frequencies()
		for key in ending_n_grams_c.keys():
			#score = len(key) * ending_n_grams_c[key].frequency ** 2 / n_grams_c[key].frequency
			freq = ending_n_grams_c[key].frequency
			#score = freq * math.exp(freq / n_grams_c[key].frequency * math.log(1.5))
			#score = (freq / n_grams_c[key].frequency) / -math.log(freq)
			#score = freq * math.exp(freq / n_grams_c[key].frequency * math.log(math.sqrt(len(key))))
			score = freq / n_grams_c[key].frequency
			#score = 1.0
			endings_c.add(key, count = ending_n_grams_c[key].count, \
				frequency = ending_n_grams_c[key].frequency, score = score)
	# add empty ending
	endings_c.add('', count = len(wordlist), frequency = 1.0, score = EMPTY_ENDING_SCORE)
	endings_c.save_to_file('endings.txt', sortkey = lambda x: x.frequency)

