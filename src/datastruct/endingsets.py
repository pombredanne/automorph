import codecs

class EndingSet:
	def __init__(self, key = '', score = 0.0):
		self.key = key
		self.count = 0
		self.frequency = 0.0
		self.score = score
	
	@staticmethod
	def endings_to_string(endings):
		return u','.join(sorted([e.orth for e in endings]))

	@staticmethod
	def string_to_endings(string, endings_c):
		endings = set([])
		for key in string.split(u','):
			try:
				endings.add(endings_c[key])
			except KeyError:
				pass
		return endings
	
	def to_string(self):
		return u'\t'.join([self.key, str(self.count), str(self.score)])
	
	@staticmethod
	def from_string(string):
		data = string.split(u'\t')
		endingset = EndingSet(data[0], float(data[2]))
		endingset.count = int(data[1])
		return endingset

class EndingSetsContainer:
	def __init__(self):
		self.endingsets = {}
		self.count = 0
	
	def __getitem__(self, key):
		return self.endingsets[key]
	
	def add(self, key, count = 1):
		try:
			self.endingsets[key].count += count
		except KeyError:
			self.endingsets[key] = EndingSet(key)
			self.endingsets[key].count += count
		self.count += count

	def save_to_file(self, filename, sortkey = lambda x: x.score):
		# create and sort ending sets list
		endingsets_list = sorted(self.endingsets.values(), reverse = True, key = sortkey)
		# write list to file
		with codecs.open(filename, 'w+', 'utf-8') as fp:
			for es in endingsets_list:
				fp.write(es.to_string() + '\n')

	@staticmethod
	def load_from_file(filename):
		endingsets_c = EndingSetsContainer()
		with codecs.open(filename, 'r', 'utf-8') as fp:
			#count = 0
			for line in fp:
				es = EndingSet.from_string(line.rstrip())
				endingsets_c.endingsets[es.key] = es
				endingsets_c.count += es.count
				#count += 1
				#if count > 1000:
				#	break
		return endingsets_c
