# -*- coding: utf-8 -*-

import endingsets
import codecs
#import pickle

class StemTrieNode:
	def __init__(self, stem, root):
		self.root = root
		self.stem = stem
		self.endingsets = [set([])]
		self.children = {}

	def __len__(self):
		result = 1
		for c in self.children.values():
			result += len(c)
		return result

	def __getitem__(self, key):
		if len(key) == 0:
			return self
		elif key[0] in self.children.keys():
			try:
				return self.children[key[0]][key[1:]]
			except KeyError:
				raise KeyError(key)
		else:
			raise KeyError(key)

	def add(self, word):
		if len(self.stem) > 0:
			try:
				self.endingsets[0] |= set([self.root.endings_container[word]])
			except KeyError:
				pass
		if len(word) > 0:
			if not word[0] in self.children.keys():
				self.children[word[0]] = StemTrieNode(self.stem + word[0], self.root)
			self.children[word[0]].add(word[1:])
	
	def list_stems(self):
		result = []
		endingset = set([])
		for es in self.endingsets:
			endingset |= es
		if len(self.stem) > 0 and len(endingset) > 0:
			result.append((self.stem, endingset))
		for c in self.children.values():
			result.extend(c.list_stems())
		return result

	def list_all_nodes(self):
		result = []
		if len(self.stem) > 0:
			result.append((self.stem, self.endingsets))
		for c in self.children.values():
			result.extend(c.list_all_nodes())
		return result
	
	def list_words(self):
		return [self.prefix + e.orth for e in self.endingset]

class StemTrie(StemTrieNode):
	def __init__(self, endings_container):
		StemTrieNode.__init__(self, '', self)
		self.endings_container = endings_container
	
	def save_to_file(self, filename):
		with codecs.open(filename, 'w+', 'utf-8') as fp:
			for stem, ess in sorted(self.list_all_nodes(), key = lambda x: x[0]):
				endings_str = '\t'.join([endingsets.EndingSet.endings_to_string(es) for es in ess])
				fp.write(stem + '\t' + endings_str + '\n')
	
	@staticmethod
	def load_from_file(filename, endings_container):
		trie = StemTrie(endings_container)
		with codecs.open(filename, 'r', 'utf-8') as fp:
			for line in fp:
				content = line.split('\t')
				node = StemTrieNode(content[0], trie)
				node.endingsets = []
				for es in content[1:]:
					node.endingsets.append(\
						endingsets.EndingSet.string_to_endings(es.strip(), endings_container))
				trie[node.stem[:-1]].children[node.stem[-1]] = node
		return trie
